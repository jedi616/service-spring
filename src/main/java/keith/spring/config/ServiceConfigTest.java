package keith.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

/**
 * 
 * @author kjayme
 *
 */
@Configuration
@ComponentScan(basePackages = { "keith.spring.maintenance.service" })
@Import(value = { DataConfig.class })
@Profile("test")
public class ServiceConfigTest {
}
