package keith.spring.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

import keith.domain.util.KeithUtils;

/**
 * 
 * @author kjayme
 *
 */
@Configuration
@ComponentScan(basePackages = { "keith.spring.maintenance.service" })
@Import(value = { DataConfig.class })
// @PropertySource("/WEB-INF/spring/application.properties")
// @PropertySource("classpath:application.properties")
@Profile("production")
public class ServiceConfig
{
    @Bean
    public PropertySourcesPlaceholderConfigurer properties(ApplicationContext applicationContext, Environment environment) throws Exception
    {
        return KeithUtils.properties(applicationContext, environment);
    }
}
