package keith.spring.maintenance.service;

import static keith.domain.util.KeithUtils.validateSelectedItem;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import keith.api.maintenance.service.StudentService;
import keith.domain.dto.CourseDto;
import keith.domain.dto.SelectItem;
import keith.domain.dto.ServiceResponse;
import keith.domain.dto.StudentDto;
import keith.domain.jpa.entity.CourseM;
import keith.domain.jpa.entity.StudentM;
import keith.spring.datajpa.mapper.CourseMapper;
import keith.spring.datajpa.mapper.StudentMapper;
import keith.spring.datajpa.repository.CourseRepository;
import keith.spring.datajpa.repository.StudentRepository;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Service
public class StudentServiceImpl implements StudentService {
    
    private static final String NOT_FOUND_ERROR_MESSAGE = "Student Not Found";
    private static final String DELETE_SUCCESS_MESSAGE = "Student is Deleted";
    private static final String CREATE_SUCCESS_MESSAGE = "Student is Created";
    private static final String UPDATE_SUCCESS_MESSAGE = "Student is Updated";

    @Autowired
    private StudentRepository studentRepo;

    @Autowired
    private CourseRepository courseRepo;

    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private CourseMapper courseMapper;

    @Override
    public ServiceResponse<List<StudentDto>> retrieveAll() throws Exception {
        ServiceResponse<List<StudentDto>> response = new ServiceResponse<List<StudentDto>>();
        try {
            List<StudentM> studentMs = studentRepo.findAll();
            // TODO: Optionally pass this to mapToDtoList method and implement
            // ListMapperListener.
            List<StudentDto> studentDtos = studentMapper.mapToDtoList(studentMs);
            response.setData(studentDtos);
        } catch (Exception e) {
            response.error(e);
        }
        return response;
    }

    @Override
    public ServiceResponse<List<StudentDto>> retrieveAll(Comparator<StudentDto> comparator) throws Exception {
        ServiceResponse<List<StudentDto>> response = retrieveAll();
        if (response.isSuccess()) {
            Collections.sort(response.getData(), comparator);
        }
        return response;
    }

    @Override
    public ServiceResponse<StudentDto> retrieveNew() throws Exception {
        ServiceResponse<StudentDto> response = new ServiceResponse<StudentDto>();
        try {
            StudentDto studentDto = new StudentDto();
            // TODO: void initializeNewDto(studentDto);
            initCourseSelectItems(studentDto);
            response.setData(studentDto);
        } catch (Exception e) {
            response.error(e);
        }
        return response;

    }

    private void initCourseSelectItems(StudentDto studentDto) throws Exception {
        List<CourseM> courseMs = courseRepo.findAll();
        List<SelectItem> courseSelectItems = courseMapper.getSelectItems(courseMs);
        studentDto.setCourseSelectItems(courseSelectItems);
        if (studentDto.getCourse() != null) {
            validateSelectedItem(studentDto.getCourse().getCourseId(), courseSelectItems);
        } else {
            if (courseSelectItems.size() > 0) {
                CourseDto courseDto = new CourseDto();
                courseDto.setCourseId(courseSelectItems.get(0).getValue());
                studentDto.setCourse(courseDto);
            }
        }
    }

    @Override
    public ServiceResponse<StudentDto> retrieveOne(long studentId) throws Exception {
        ServiceResponse<StudentDto> response = new ServiceResponse<StudentDto>();
        try {
            StudentDto studentDto = null;
            StudentM studentM = studentRepo.findOne(studentId);
            if (studentM != null) {
                // TODO: void validateEdit(studentM, serviceResponse);
                // TODO: if (serviceResponse.getStatus() == Status.ERROR) return
                // serviceResponse;
                studentDto = new StudentDto();
                // TODO: void preEditDto(studentDto, studentM);
                studentMapper.mapToDto(studentDto, studentM);
                // TODO: void postEditDto(studentDto);
                initCourseSelectItems(studentDto);
                response.setData(studentDto);
            } else {
                response.error(NOT_FOUND_ERROR_MESSAGE);
            }
        } catch (Exception e) {
            response.error(e);
        }
        return response;
    }

    @Override
    public ServiceResponse<StudentDto> save(StudentDto studentDto) throws Exception {
        ServiceResponse<StudentDto> response = new ServiceResponse<StudentDto>();
        try {
            StudentM studentM;
            if (studentDto.getStudentId() == 0) {
                // TODO: call void validateCreate(studentDto, response);
                // TODO: if (response.getStatus() == Status.ERROR) return response;
                studentM = studentMapper.mapToEntity(studentDto);
                StudentDto savedStudentDto = save(studentM);
                response.success(savedStudentDto, CREATE_SUCCESS_MESSAGE);
            } else {
                studentM = studentRepo.findOne(studentDto.getStudentId());
                if (studentM != null) {
                    // TODO: call void validateUpdate(studentM, studentDto, response);
                    // TODO: if (response.getStatus() == Status.ERROR) return response;
                    studentMapper.mapToEntity(studentM, studentDto);
                    StudentDto savedStudentDto = save(studentM);
                    response.success(savedStudentDto, UPDATE_SUCCESS_MESSAGE);
                } else {
                    response.error(NOT_FOUND_ERROR_MESSAGE);
                }
            }
        } catch (Exception e) {
            response.error(e);
        }
        return response;
    }

    private StudentDto save(StudentM studentM) throws Exception {
        studentM = studentRepo.save(studentM);
        // TODO: void saveDetails(studentM);
        return studentMapper.mapToDto(studentM);
    }

    @Override
    public ServiceResponse<StudentDto> delete(long studentId) throws Exception {
        ServiceResponse<StudentDto> response = new ServiceResponse<StudentDto>();
        try {
            StudentM studentM = studentRepo.findOne(studentId);
            if (studentM != null) {
                // TODO: void validateDelete(studentM, serviceResponse);
                // TODO: if (serviceResponse.getStatus() == Status.ERROR) return
                // serviceResponse;
                StudentDto deletedStudentDto = studentMapper.mapToDto(studentM);
                studentRepo.delete(studentM);
                response.success(deletedStudentDto, DELETE_SUCCESS_MESSAGE);
            } else {
                response.error(NOT_FOUND_ERROR_MESSAGE);
            }
        } catch (Exception e) {
            response.error(e);
        }
        return response;
    }
}
