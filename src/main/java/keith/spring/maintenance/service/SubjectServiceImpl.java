package keith.spring.maintenance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import keith.api.maintenance.service.SubjectService;
import keith.domain.dto.ServiceResponse;
import keith.domain.dto.SubjectDto;
import keith.domain.jpa.entity.SubjectM;
import keith.spring.datajpa.mapper.SubjectMapper;
import keith.spring.datajpa.repository.SubjectRepository;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Service
public class SubjectServiceImpl implements SubjectService {
    
    private static final String NOT_FOUND_ERROR_MESSAGE = "Subject Not Found";

    @Autowired
    private SubjectRepository repo;

    @Autowired
    private SubjectMapper mapper;

    @Override
    public ServiceResponse<List<SubjectDto>> retrieveAll() throws Exception {
        ServiceResponse<List<SubjectDto>> response = new ServiceResponse<List<SubjectDto>>();
        try {
            List<SubjectM> subjectMs = repo.findAll();
            List<SubjectDto> subjectDtos = mapper.mapToDtoList(subjectMs);
            response.setData(subjectDtos);
        } catch (Exception e) {
            response.error(e);
        }
        return response;
    }

    @Override
    public ServiceResponse<SubjectDto> retrieveOne(long subjectId) throws Exception {
        ServiceResponse<SubjectDto> response = new ServiceResponse<SubjectDto>();
        try {
            SubjectM subjectM = repo.findOne(subjectId);
            if (subjectM != null) {
                SubjectDto subjectDto = mapper.mapToDto(subjectM);
                response.setData(subjectDto);
            } else {
                response.error(NOT_FOUND_ERROR_MESSAGE);
            }
        } catch (Exception e) {
            response.error(e);
        }
        return response;
    }

    @Override
    public ServiceResponse<SubjectDto> save(SubjectDto subjectDto) throws Exception {
        ServiceResponse<SubjectDto> response = new ServiceResponse<SubjectDto>();
        try {
            // Insert only, not yet handle Updates.
            SubjectM subjectM = mapper.mapToEntity(subjectDto);
            subjectM = repo.save(subjectM);
            SubjectDto savedSubjectDto = mapper.mapToDto(subjectM);
            response.setData(savedSubjectDto);
        } catch (Exception e) {
            response.error(e);
        }
        return response;
    }

    @Override
    @Transactional
    public ServiceResponse<List<SubjectDto>> saveAll(List<SubjectDto> subjectDtos) throws Exception {
        ServiceResponse<List<SubjectDto>> response = new ServiceResponse<List<SubjectDto>>();
        try {
            List<SubjectM> subjectMs = mapper.mapToEntityList(subjectDtos);
            subjectMs = repo.save(subjectMs);
            List<SubjectDto> savedSubjectDtos = mapper.mapToDtoList(subjectMs);
            response.setData(savedSubjectDtos);

            /*
             * List<SubjectM> subjectMs = new ArrayList<SubjectM>(); for (SubjectDto
             * subjectDto : subjectDtos) { SubjectM subjectM; if (subjectDto.getSubjectId()
             * == 0) { subjectM = mapper.mapToEntity(subjectDto); } else { subjectM =
             * repo.findOne(subjectDto.getSubjectId()); mapper.mapToEntity(subjectDto,
             * subjectM); } subjectMs.add(subjectM); } repo.save(subjectMs);
             */
        } catch (Exception e) {
            response.error(e);
        }
        return response;

    }

    @Override
    public ServiceResponse<SubjectDto> delete(long subjectId) throws Exception {
        ServiceResponse<SubjectDto> response = new ServiceResponse<SubjectDto>();
        try {
            // No checking yet if subject to be deleted exists.
            repo.delete(subjectId);
        } catch (Exception e) {
            response.error(e);
        }
        return response;
    }
}
