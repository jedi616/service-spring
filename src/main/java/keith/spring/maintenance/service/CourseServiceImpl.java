package keith.spring.maintenance.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import keith.api.maintenance.service.CourseService;
import keith.domain.dto.CourseDto;
import keith.domain.dto.ServiceResponse;
import keith.domain.jpa.entity.CourseM;
import keith.spring.datajpa.mapper.CourseMapper;
import keith.spring.datajpa.repository.CourseRepository;
import keith.spring.datajpa.repository.StudentRepository;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Service
public class CourseServiceImpl implements CourseService {
    
    private static final String NOT_FOUND_ERROR_MESSAGE = "Course Not Found";
    private static final String USED_BY_STUDENT_ERROR_MESSAGE = "Course is being used by a Student";
    private static final String COURSE_ALREADY_EXISTS_ERROR_MESSAGE = "Course already exists";
    private static final String DELETE_SUCCESS_MESSAGE = "Course is Deleted";
    private static final String CREATE_SUCCESS_MESSAGE = "Course is Created";
    private static final String UPDATE_SUCCESS_MESSAGE = "Course is Updated";

    @Autowired
    private CourseRepository courseRepo;

    @Autowired
    private StudentRepository studentRepo;

    @Autowired
    private CourseMapper courseMapper;

    @Override
    public ServiceResponse<List<CourseDto>> retrieveAll() throws Exception {
        ServiceResponse<List<CourseDto>> response = new ServiceResponse<List<CourseDto>>();
        try {
            List<CourseM> courseMs = courseRepo.findAll();
            // TODO: Optionally pass this to mapToDtoList method and implement
            // ListMapperListener.
            List<CourseDto> courseDtos = courseMapper.mapToDtoList(courseMs);
            response.setData(courseDtos);
        } catch (Exception e) {
            response.error(e);
        }
        return response;
    }

    @Override
    public ServiceResponse<List<CourseDto>> retrieveAll(Comparator<CourseDto> comparator) throws Exception {
        ServiceResponse<List<CourseDto>> response = retrieveAll();
        if (response.isSuccess()) {
            Collections.sort(response.getData(), comparator);
        }
        return response;
    }

    @Override
    public ServiceResponse<CourseDto> retrieveNew() throws Exception {
        ServiceResponse<CourseDto> response = new ServiceResponse<CourseDto>();
        try {
            CourseDto courseDto = new CourseDto();
            // TODO: void initializeNewDto(courseDto);
            response.setData(courseDto);
        } catch (Exception e) {
            response.error(e);
        }
        return response;
    }

    @Override
    public ServiceResponse<CourseDto> retrieveOne(long courseId) throws Exception {
        ServiceResponse<CourseDto> response = new ServiceResponse<CourseDto>();
        try {
            CourseDto courseDto = null;
            CourseM courseM = courseRepo.findOne(courseId);
            if (courseM != null) {
                // TODO: void validateEdit(courseM, response);
                // TODO: if (response.getStatus() == Status.ERROR) return response;
                courseDto = new CourseDto();
                // TODO: void preEditDto(courseDto, courseM);
                courseMapper.mapToDto(courseDto, courseM);
                // TODO: void postEditDto(courseDto);
                response.setData(courseDto);
            } else {
                response.error(NOT_FOUND_ERROR_MESSAGE);
            }
        } catch (Exception e) {
            response.error(e);
        }
        return response;
    }

    @Override
    public ServiceResponse<CourseDto> save(CourseDto courseDto) throws Exception {
        ServiceResponse<CourseDto> response = new ServiceResponse<CourseDto>();
        try {
            CourseM courseM;
            if (courseDto.getCourseId() == 0) {
                validateCreate(courseDto, response);
                if (response.isSuccess()) {
                    courseM = courseMapper.mapToEntity(courseDto);
                    CourseDto savedCourseDto = save(courseM);
                    response.success(savedCourseDto, CREATE_SUCCESS_MESSAGE);
                }
            } else {
                courseM = courseRepo.findOne(courseDto.getCourseId());
                if (courseM != null) {
                    validateUpdate(courseM, courseDto, response);
                    if (response.isSuccess()) {
                        courseMapper.mapToEntity(courseM, courseDto);
                        CourseDto savedCourseDto = save(courseM);
                        response.success(savedCourseDto, UPDATE_SUCCESS_MESSAGE);
                    }
                } else {
                    response.error(NOT_FOUND_ERROR_MESSAGE);
                }
            }
        } catch (Exception e) {
            response.error(e);
        }
        return response;
    }

    private void validateCreate(CourseDto courseDto, ServiceResponse<CourseDto> response) {
        if (courseRepo.findByCourseName(courseDto.getCourseName()).size() > 0) {
            response.error(COURSE_ALREADY_EXISTS_ERROR_MESSAGE);
        }
    }

    private void validateUpdate(CourseM courseM, CourseDto courseDto, ServiceResponse<CourseDto> response) {
        if (courseRepo.findByCourseNameAndCourseIdNot(courseDto.getCourseName(), courseM.getCourseId()).size() > 0) {
            response.error(COURSE_ALREADY_EXISTS_ERROR_MESSAGE);
        }
    }

    private CourseDto save(CourseM courseM) throws Exception {
        courseM = courseRepo.save(courseM);
        // TODO: void saveDetails(studentM);
        return courseMapper.mapToDto(courseM);
    }

    @Override
    public ServiceResponse<CourseDto> delete(long courseId) throws Exception {
        ServiceResponse<CourseDto> response = new ServiceResponse<CourseDto>();
        try {
            CourseM courseM = courseRepo.findOne(courseId);
            if (courseM != null) {
                validateDelete(courseM, response);
                if (response.isSuccess()) {
                    CourseDto deletedCourseDto = courseMapper.mapToDto(courseM);
                    courseRepo.delete(courseM);
                    response.success(deletedCourseDto, DELETE_SUCCESS_MESSAGE);
                }
            } else {
                response.error(NOT_FOUND_ERROR_MESSAGE);
            }
        } catch (Exception e) {
            response.error(e);
        }
        return response;
    }

    private void validateDelete(CourseM courseM, ServiceResponse<CourseDto> response) {
        if (studentRepo.findByCourseM(courseM).size() > 0) {
            response.error(USED_BY_STUDENT_ERROR_MESSAGE);
        }
    }
}
