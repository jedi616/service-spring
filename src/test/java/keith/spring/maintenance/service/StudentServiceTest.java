package keith.spring.maintenance.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.Assert;

import keith.domain.dto.ServiceResponse;
import keith.domain.dto.StudentDto;
import keith.domain.jpa.entity.StudentM;
import keith.spring.datajpa.mapper.StudentMapper;
import keith.spring.datajpa.repository.StudentRepository;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class StudentServiceTest {
    
    @InjectMocks
    private StudentServiceImpl service;

    @Mock
    private StudentRepository mockStudentRepository;

    @Mock
    private StudentMapper mockStudentMapper;

    @Test
    @SuppressWarnings("unchecked")
    public void testRetrieveAll() throws Exception {
        List<StudentM> studentEntities = prepareStudentEntities();
        Mockito.when(mockStudentRepository.findAll()).thenReturn(studentEntities);
        List<StudentDto> studentDtos = prepareStudentDtos();
        Mockito.when(mockStudentMapper.mapToDtoList(Mockito.isA(List.class))).thenReturn(studentDtos);
        ServiceResponse<List<StudentDto>> serviceResponse = service.retrieveAll();
        Assert.notEmpty(serviceResponse.getData());
    }

    private List<StudentM> prepareStudentEntities() {
        List<StudentM> studentEntities = new ArrayList<StudentM>();
        StudentM studentM = new StudentM();
        studentM.setStudentId(1L);
        studentM.setName("a");
        studentEntities.add(studentM);
        studentM = new StudentM();
        studentM.setStudentId(2L);
        studentM.setName("b");
        studentEntities.add(studentM);
        return studentEntities;
    }

    private List<StudentDto> prepareStudentDtos() {
        List<StudentDto> studentDtos = new ArrayList<StudentDto>();
        StudentDto studentDto = new StudentDto();
        studentDto.setStudentId(1L);
        studentDto.setName("a");
        studentDtos.add(studentDto);
        studentDto = new StudentDto();
        studentDto.setStudentId(2L);
        studentDto.setName("b");
        studentDtos.add(studentDto);
        return studentDtos;
    }
}
